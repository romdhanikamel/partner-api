# Partner API

## Running

You can start the application directly from your preferred IDE as Spring Boot application.
Point your browser or REST client to `http://localhost:8080/actuator/health`.
You should get a `200 OK` status response with **status: Up** text.

## Using a REST Client

To test some available end-points, you can use any REST client:

- [Postman](https://www.postman.com/)
- [Talend Chrome extension](https://www.talend.com/)
- [Firefox RESTClient](http://restclient.net/), etc.).

If you use Postman, you can import a ready-to-use collection configuration from src\main\resources\static\partners.postman_collection.json.