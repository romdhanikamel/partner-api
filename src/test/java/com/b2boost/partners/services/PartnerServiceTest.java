package com.b2boost.partners.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.b2boost.partners.exceptions.BadRequestException;
import com.b2boost.partners.exceptions.PartnerNotFoundException;
import com.b2boost.partners.models.Partner;
import com.b2boost.partners.models.PartnerCandidate;
import com.b2boost.partners.repositories.PartnerRepository;

@SpringBootTest
class PartnerServiceTest {

	@MockBean
	PartnerRepository partnerRepository;

	@Autowired
	PartnerService partnerService;

	@Test
	void testFindAndExpectPartner() throws ParseException {
		Partner partner = BuildPartner();

		given(partnerRepository.findById(Long.valueOf(1))).willReturn(Optional.of(partner));

		assertThat(partnerService.find(Long.valueOf(1))).isEqualTo(partner);
	}

	@Test
	void testFindAndExpectPartnerNotFoundException() {
		Mockito.when(partnerRepository.findById(Long.valueOf(1))).thenReturn(Optional.empty());

		Throwable exception = Assertions.assertThrows(PartnerNotFoundException.class, () -> {
			partnerService.find(Long.valueOf(1));
		});
		assertEquals("Partner not found", exception.getMessage());

	}

	@Test
	void testSaveAndExpectPartnerAlreadyExistsException() throws ParseException {
		Partner partner = BuildPartner();
		given(partnerRepository.findByRef("xxxxxx4")).willReturn(Optional.of(partner));
		Throwable exception = Assertions.assertThrows(BadRequestException.class, () -> {
			partnerService.save(BuildPartnerCandidate());
		});

		assertEquals("partner with reference: xxxxxx4 already exists", exception.getMessage());
	}

	@Test
	void testSaveWithNullAttributeAndExpectBadRequestException() throws ParseException {
		PartnerCandidate partnerCandidate = BuildPartnerCandidate();
		partnerCandidate.setName(null);
		Throwable exception = Assertions.assertThrows(BadRequestException.class, () -> {
			partnerService.save(partnerCandidate);
		});

		assertEquals("All attributes must be different from null", exception.getMessage());
	}

	private PartnerCandidate BuildPartnerCandidate() throws ParseException {
		String sDate1 = "09/10/2020";
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		return new PartnerCandidate("Sara Vasquez", "xxxxxx4", Locale.FRANCE, date);
	}

	private Partner BuildPartner() throws ParseException {
		String sDate1 = "09/10/2020";
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		return new Partner(Long.valueOf(1), "Sara Vasquez", "xxxxxx4", Locale.FRANCE, date);
	}

}
