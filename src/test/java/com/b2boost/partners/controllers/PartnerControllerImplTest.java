package com.b2boost.partners.controllers;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.b2boost.partners.exceptions.PartnerNotFoundException;
import com.b2boost.partners.models.Partner;
import com.b2boost.partners.services.PartnerService;

@WebMvcTest(PartnerControllerImpl.class)
class PartnerControllerImplTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private PartnerService partnerService;

	@Test
	void testGetByIdAndExpectOk() throws Exception {
		Partner partner = BuildPartner();
		given(partnerService.find(Long.valueOf(1))).willReturn(partner);

		mvc.perform(get("/api/partners/{id}", 1)).andDo(print()).andExpect(status().isOk());
	}

	@Test
	void testGetByIdAndExpectNotFound() throws Exception {
		PartnerNotFoundException partnerNotFoundException = new PartnerNotFoundException();

		given(partnerService.find(Long.valueOf(1))).willThrow(partnerNotFoundException);

		mvc.perform(get("/api/partners/{id}", 1)).andDo(print()).andExpect(status().isNotFound());
	}

	private Partner BuildPartner() throws ParseException {
		String sDate1 = "09/10/2020";
		Date date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		return new Partner(Long.valueOf(1), "Sara Vasquez", "xxxxxx4", Locale.FRANCE, date);
	}

}
