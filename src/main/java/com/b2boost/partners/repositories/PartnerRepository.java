package com.b2boost.partners.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.b2boost.partners.models.Partner;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, Long> {

	Optional<Partner> findByRef(String ref);

}
