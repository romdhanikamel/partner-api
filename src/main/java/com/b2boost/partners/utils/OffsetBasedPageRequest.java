package com.b2boost.partners.utils;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.b2boost.partners.exceptions.BadRequestException;

public class OffsetBasedPageRequest implements Pageable {

	private int size = 10;

	private int offset = 0;

	private Sort sort = Sort.by(Sort.Direction.ASC, "id");

	public OffsetBasedPageRequest(int limit, int offset) {
		if (limit < 1) {
			throw new BadRequestException("Limit must not be less than one!");
		}
		if (offset < 0) {
			throw new BadRequestException("Offset index must not be less than zero!");
		}
		this.size = limit;
		this.offset = offset;
	}

	@Override
	public int getPageNumber() {
		return offset / size;
	}

	@Override
	public int getPageSize() {
		return size;
	}

	@Override
	public long getOffset() {
		return offset;
	}

	@Override
	public Sort getSort() {
		return sort;
	}

	@Override
	public Pageable next() {
		return new OffsetBasedPageRequest(getPageSize(), (int) (getOffset() + getPageSize()));
	}

	public Pageable previous() {
		return hasPrevious() ? new OffsetBasedPageRequest(getPageSize(), (int) (getOffset() - getPageSize())) : this;
	}

	@Override
	public Pageable previousOrFirst() {
		return hasPrevious() ? previous() : first();
	}

	@Override
	public Pageable first() {
		return new OffsetBasedPageRequest(getPageSize(), 0);
	}

	@Override
	public boolean hasPrevious() {
		return offset > size;
	}
}
