package com.b2boost.partners.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.b2boost.partners.exceptions.BadRequestException;
import com.b2boost.partners.exceptions.PartnerNotFoundException;
import com.b2boost.partners.models.Partner;
import com.b2boost.partners.models.PartnerCandidate;
import com.b2boost.partners.repositories.PartnerRepository;
import com.b2boost.partners.utils.OffsetBasedPageRequest;

@Service
@Transactional
public class PartnerService {

	@Autowired
	PartnerRepository partnerRepository;

	public Partner find(Long id) {
		return partnerRepository.findById(id).orElseThrow(PartnerNotFoundException::new);

	}

	public Partner save(PartnerCandidate partnerCandidate) {
		isValid(partnerCandidate);
		Partner partner = partnerCandidateToPartner(partnerCandidate);
		return partnerRepository.save(partner);

	}

	private void isValid(PartnerCandidate partnerCandidate) {
		if (partnerCandidate.getName() == null || partnerCandidate.getExpirationTime() == null
				|| partnerCandidate.getLocale() == null || partnerCandidate.getReference() == null)
			throw new BadRequestException("All attributes must be different from null");

		if (partnerRepository.findByRef(partnerCandidate.getReference()).isPresent())
			throw new BadRequestException(
					"partner with reference: " + partnerCandidate.getReference() + " already exists");

	}

	private Partner partnerCandidateToPartner(PartnerCandidate partnerCandidate) {
		return new Partner(partnerCandidate.getName(), partnerCandidate.getReference(), partnerCandidate.getLocale(),
				partnerCandidate.getExpirationTime());
	}

	public List<Partner> findAll(int offset, int size) {
		Pageable pageable = new OffsetBasedPageRequest(size, offset);
		return partnerRepository.findAll(pageable).getContent();
	}

	public Partner update(Long id, PartnerCandidate partnerCandidate) {
		Partner partner = partnerRepository.findById(id).orElseThrow(PartnerNotFoundException::new);
		partner.setExpires(partnerCandidate.getExpirationTime());
		partner.setLocale(partnerCandidate.getLocale());
		partner.setRef(partnerCandidate.getReference());
		partner.setCompanyName(partnerCandidate.getName());
		return partnerRepository.save(partner);
	}

	public void delete(Long id) {
		partnerRepository.findById(id).orElseThrow(PartnerNotFoundException::new);
		partnerRepository.deleteById(id);
		return;
	}

}
