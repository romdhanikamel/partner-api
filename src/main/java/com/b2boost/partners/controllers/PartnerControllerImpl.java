package com.b2boost.partners.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.b2boost.partners.models.Partner;
import com.b2boost.partners.models.PartnerCandidate;
import com.b2boost.partners.services.PartnerService;

@RestController
public class PartnerControllerImpl implements PartnerController {

	@Autowired
	PartnerService partnerService;

	@Override
	public List<Partner> getAll(int from, int size) {
		return partnerService.findAll(from, size);
	}

	@Override
	public Partner getById(Long id) {
		return partnerService.find(id);

	}

	@Override
	public Partner create(PartnerCandidate partnerCandidate) {
		return partnerService.save(partnerCandidate);
	}

	@Override
	public Partner update(Long id, PartnerCandidate partnerCandidate) {
		return partnerService.update(id, partnerCandidate);
	}

	@Override
	public void delete(Long id) {
		partnerService.delete(id);
		return;
	}

}
