package com.b2boost.partners.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.b2boost.partners.models.Partner;
import com.b2boost.partners.models.PartnerCandidate;

@RequestMapping("/api/partners")
public interface PartnerController {

	@GetMapping()
	List<Partner> getAll(@RequestParam(defaultValue = "0") int from, @RequestParam(defaultValue = "10") int size);

	@GetMapping("/{id}")
	Partner getById(@PathVariable Long id);

	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	Partner create(@RequestBody PartnerCandidate partnerCandidate);

	@PutMapping("/{id}")
	Partner update(@PathVariable Long id, @RequestBody PartnerCandidate partnerCandidate);

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id);
}
