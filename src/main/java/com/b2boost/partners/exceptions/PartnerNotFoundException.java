package com.b2boost.partners.exceptions;

public class PartnerNotFoundException extends ApplicationException {

	public PartnerNotFoundException() {
		super("Partner not found");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1645161050825551447L;

}
