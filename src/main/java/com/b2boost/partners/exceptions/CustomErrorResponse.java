package com.b2boost.partners.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomErrorResponse {

	private int code;
	private String message;

}
