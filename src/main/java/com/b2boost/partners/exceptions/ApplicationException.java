package com.b2boost.partners.exceptions;

public abstract class ApplicationException extends RuntimeException {
	ApplicationException(String message) {
		super(message);
	}
}
