package com.b2boost.partners.exceptions;

public class BadRequestException extends ApplicationException {
	public BadRequestException(String message) {
		super(message);
	}
}
