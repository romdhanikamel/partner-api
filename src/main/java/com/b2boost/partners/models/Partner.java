package com.b2boost.partners.models;

import java.util.Date;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Partner {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	@NotNull
	String companyName;
	@NotNull
	String ref;
	@NotNull
	Locale locale;
	@NotNull
	Date expires;

	public Partner(String companyName, String ref, Locale locale, Date expires) {
		this.companyName = companyName;
		this.ref = ref;
		this.locale = locale;
		this.expires = expires;
	}

}
