package com.b2boost.partners.models;

import java.util.Date;
import java.util.Locale;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PartnerCandidate {

	String name;
	String reference;
	Locale locale;
	Date expirationTime;
}
